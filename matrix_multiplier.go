package main

import (
	"fmt"
)

type matrix struct {
	a [][]int
}

func NewMatrix(y, x int) matrix {
	a := make([][]int, y)
	for i := range a {
		a[i] = make([]int, x)
	}
	return matrix{a}
}

func (m *matrix) rows() int {
	return len(m.a)
}

func (m *matrix) columns() int {
	return len(m.a[0])
}

func (m matrix) String() string {
	var buf string
	for _, elem := range m.a {
		buf += fmt.Sprintf("%v\n", elem)
	}
	return string(buf)
}

func Multiply(a, b matrix) matrix {
	if a.columns() != b.rows() {
		panic("a column size != b row size")
	}
	res := NewMatrix(a.rows(), b.columns())
	for i := 0; i < a.rows(); i++ {
		for j := 0; j < b.columns(); j++ {
			for k := 0; k < a.columns(); k++ {
				res.a[i][j] = res.a[i][j] + a.a[i][k]*b.a[k][j]
			}
		}
	}
	return res
}

func MultiplyVinograd(a, b matrix) matrix {

	x := a.rows()
	y := a.columns()
	z := b.columns()
	if a.columns() != b.rows() {
		panic("a column size != b row size")
	}
	// 1
	res := NewMatrix(a.rows(), b.columns())
	var mulH = make([]int, a.rows())
	for i := 0; i < a.rows(); i++ {
		for k := 0; k < a.columns()/2; k++ {
			mulH[i] = mulH[i] + a.a[i][2*k]*a.a[i][2*k+1]
		}
	}
	// 2
	var mulV = make([]int, b.columns())
	for i := 0; i < b.columns(); i++ {
		for k := 0; k < b.rows()/2; k++ {
			mulV[i] = mulV[i] + b.a[2*k][i]*b.a[2*k+1][i]
		}
	}
	//3
	for i := 0; i < a.rows(); i++ {
		for j := 0; j < b.columns(); j++ {
			res.a[i][j] -= (mulH[i] + mulV[j])
			for k := 0; k < a.columns()/2; k++ {
				res.a[i][j] = res.a[i][j] + (a.a[i][2*k]+b.a[2*k+1][j])*(a.a[i][2*k+1]+b.a[2*k][j])
			}
		}
	}
	if y%2 == 1 {
		for i := 0; i < x; i++ {
			for j := 0; j < z; j++ {
				res.a[i][j] = res.a[i][j] + a.a[i][y-1]*b.a[y-1][j]
			}
		}
	}
	return res
}

func MultiplyVinogradOptimized(a, b matrix) matrix {

	if a.columns() != b.rows() {
		panic("a column size != b row size")
	}
	x := a.rows()
	y := a.columns()
	z := b.columns()

	// 1
	res := NewMatrix(x, z)
	var mulH = make([]int, x)
	for i := 0; i < x; i++ {
		for k := 1; k < y; k+=2 {
			mulH[i] -= a.a[i][k-1]*a.a[i][k]
		}
	}
	// 2
	var mulV = make([]int, z)
	for i := 0; i < z; i++ {
		for k := 1; k < y; k+=2 {
			mulV[i] -= b.a[k-1][i]*b.a[k][i]
		}
	}
	//3
	for i := 0; i < x; i++ {
		for j := 0; j < z; j++ {
			res.a[i][j] += mulH[i] + mulV[j]
			for k := 1; k < y; k+=2 {
				res.a[i][j] += (a.a[i][k-1]+b.a[k][j])*(a.a[i][k]+b.a[k-1][j])
			}
		}
	}
	if y%2 == 1 {
		for i := 0; i < x; i++ {
			for j := 0; j < z; j++ {
				res.a[i][j] += a.a[i][y-1]*b.a[y-1][j]
			}
		}
	}
	return res
}

func main() {
	a := matrix{[][]int{
		{1, 2, 3},
		{6, 7, 8},
	}}
	b := matrix{[][]int{
		{1, 2},
		{3, 4},
		{5, 6},
	}}
	res := MultiplyVinogradOptimized(a,b)
	fmt.Print(res)
}