package main

import (
	"fmt"
	"math/rand"
	"reflect"
	"testing"
)

func TestNewMatrix(t *testing.T) {
	m := NewMatrix(2, 3)
	if len(m.a) != 2 || len(m.a[0]) != 3 {
		t.Fatal()
	}
}

func TestMatrixColumn(t *testing.T) {
	mc := NewMatrix(2, 1)
	want := matrix{[][]int{
		{0},
		{0},
	}}
	if !reflect.DeepEqual(want, mc) {
		t.Fatal()
	}
}

func TestEqualMatrix(t *testing.T) {
	a := NewMatrix(2, 3)
	b := NewMatrix(2, 3)
	a.a[1][1] = 2
	b.a[1][1] = 2
	if !reflect.DeepEqual(a, b) {
		t.Fatal()
	}
	a = NewMatrix(1, 1)
	b = NewMatrix(1, 2)
	if reflect.DeepEqual(a, b) {
		t.Fatal()
	}
	a = NewMatrix(2, 1)
	b = NewMatrix(1, 1)
	if reflect.DeepEqual(a, b) {
		t.Fatal()
	}
	a = NewMatrix(2, 3)
	b = NewMatrix(2, 3)
	a.a[1][1] = 2
	b.a[1][1] = 3
	if reflect.DeepEqual(a, b) {
		t.Fatal()
	}
}

func TestColumnsRows(t *testing.T) {
	m := matrix{[][]int{
		{3, 1, 0},
		{2, -1, 5},
	}}
	if m.columns() != 3 {
		t.Fatal()
	}
	if m.rows() != 2 {
		t.Fatal()
	}
}

func TestMultiply(t *testing.T) {
	a := matrix{[][]int{
		{2, -1},
		{5, 3},
	}}
	b := matrix{[][]int{
		{3, 1, 0},
		{2, -1, 5},
	}}
	res := Multiply(a, b)
	wants := matrix{[][]int{
		{4, 3, -5},
		{21, 2, 15},
	}}
	if !reflect.DeepEqual(res, wants) {
		t.Fatal()
	}

	a = matrix{[][]int{
		{1, 2, 3},
		{-1, 0, -2},
	}}
	b = matrix{[][]int{
		{1},
		{2},
		{3},
	}}
	res = Multiply(a, b)
	wants = matrix{[][]int{
		{14},
		{-7},
	}}
	if !reflect.DeepEqual(res, wants) {
		t.Fatal()
	}

	a = matrix{[][]int{
		{1, 2, 3},
		{-1, 0, -2},
	}}
	b = matrix{[][]int{
		{1, 2},
		{3, 4},
		{5, 6},
	}}
	res = Multiply(a, b)
	wants = matrix{[][]int{
		{22, 28},
		{-11, -14},
	}}
	if !reflect.DeepEqual(res, wants) {
		t.Fatal()
	}
}

func TestMultiplyVinograd(t *testing.T) {
	a := matrix{[][]int{
		{2, -1},
		{5, 3},
	}}
	b := matrix{[][]int{
		{3, 1, 0},
		{2, -1, 5},
	}}
	res := MultiplyVinograd(a, b)
	wants := matrix{[][]int{
		{4, 3, -5},
		{21, 2, 15},
	}}
	if !reflect.DeepEqual(res, wants) {
		t.Fatal()
	}
	a = matrix{[][]int{
		{1, 2, 3},
		{6, 7, 8},
	}}
	b = matrix{[][]int{
		{1, 2},
		{3, 4},
		{5, 6},
	}}
	wants = matrix{[][]int{
		{22, 28},
		{67, 88},
	}}
	res = MultiplyVinograd(a, b)
	if !reflect.DeepEqual(res, wants) {
		t.Fatal()
	}
}

func TestSameMultiply(t *testing.T) {

	a := matrix{[][]int{
		{1, 2, 3},
		{6, 7, 8},
	}}
	b := matrix{[][]int{
		{1, 2},
		{3, 4},
		{5, 6},
	}}
	if !reflect.DeepEqual(Multiply(a, b), MultiplyVinograd(a, b)) {
		t.Fatal()
	}
}

func TestSameMultiplyOptimized(t *testing.T) {

	a := matrix{[][]int{
		{1, 2, 3},
		{6, 7, 8},
	}}
	b := matrix{[][]int{
		{1, 2},
		{3, 4},
		{5, 6},
	}}
	vo := MultiplyVinogradOptimized(a, b)
	fmt.Print(vo)
	if !reflect.DeepEqual(Multiply(a, b), vo) {
		t.Fatal()
	}
}

func createRandomSquareMatrix(n int) matrix {
	res := NewMatrix(n, n)
	for i:=0; i<n; i++ {
		for j:=0; j<n; j++ {
			res.a[i][j] = rand.Int()
		}
	}
	return res
}

func matrixMultiplyBenchmark(mm func(matrix, matrix) matrix, n int, b *testing.B) {
	m1 := createRandomSquareMatrix(n)
	m2 := createRandomSquareMatrix(n)
	for i := 0; i < b.N; i++ {
		mm(m1, m2)
	}
}

func BenchmarkMultiplySimple100(b *testing.B) {
	matrixMultiplyBenchmark(Multiply, 100, b)
}
func BenchmarkMultiplySimple200(b *testing.B) {
	matrixMultiplyBenchmark(Multiply, 200, b)
}
func BenchmarkMultiplySimple300(b *testing.B) {
	matrixMultiplyBenchmark(Multiply, 300, b)
}
func BenchmarkMultiplySimple400(b *testing.B) {
	matrixMultiplyBenchmark(Multiply, 400, b)
}
func BenchmarkMultiplySimple500(b *testing.B) {
	matrixMultiplyBenchmark(Multiply, 500, b)
}
func BenchmarkMultiplyVinograd100(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinograd, 100, b)
}
func BenchmarkMultiplyVinograd200(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinograd, 200, b)
}
func BenchmarkMultiplyVinograd300(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinograd, 300, b)
}
func BenchmarkMultiplyVinograd400(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinograd, 400, b)
}
func BenchmarkMultiplyVinograd500(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinograd, 500, b)
}
func BenchmarkMultiplyVinogradOptimized100(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinogradOptimized, 100, b)
}
func BenchmarkMultiplyVinogradOptimized200(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinogradOptimized, 200, b)
}
func BenchmarkMultiplyVinogradOptimized300(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinogradOptimized, 300, b)
}
func BenchmarkMultiplyVinogradOptimized400(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinogradOptimized, 400, b)
}
func BenchmarkMultiplyVinogradOptimized500(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinogradOptimized, 500, b)
}

func BenchmarkMultiplyVinograd101(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinograd, 101, b)
}
func BenchmarkMultiplyVinograd201(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinograd, 201, b)
}
func BenchmarkMultiplyVinograd301(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinograd, 301, b)
}
func BenchmarkMultiplyVinograd401(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinograd, 401, b)
}
func BenchmarkMultiplyVinograd501(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinograd, 501, b)
}
func BenchmarkMultiplyVinogradOptimized101(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinogradOptimized, 101, b)
}
func BenchmarkMultiplyVinogradOptimized201(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinogradOptimized, 201, b)
}
func BenchmarkMultiplyVinogradOptimized301(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinogradOptimized, 301, b)
}
func BenchmarkMultiplyVinogradOptimized401(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinogradOptimized, 401, b)
}
func BenchmarkMultiplyVinogradOptimized501(b *testing.B) {
	matrixMultiplyBenchmark(MultiplyVinogradOptimized, 501, b)
}